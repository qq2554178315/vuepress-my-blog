import { defineUserConfig } from 'vuepress'
import { defaultTheme } from '@vuepress/theme-default'
import { backToTopPlugin } from '@vuepress/plugin-back-to-top'

export default defineUserConfig({
  base: '/vuepress-my-blog/',
  lang: 'zh-CN',
  title: '你好， VuePress ！',
  description: '这是我的第一个 VuePress 站点',
  plugins: [
    backToTopPlugin(),
  ],
  theme: defaultTheme({
    // Public 文件路径
    logo: '/images/logo.png',
    // URL
    // logo: 'https://vuejs.org/images/logo.png',
    navbar: [
      // NavbarItem
      // {
      //   text: 'Foo',
      //   link: '/foo/',
      // },
      // NavbarGroup
      {
        text: '随笔',
        children: ['/group/CHATGPT问答.md', '/group/自我反思.md'],
      },
      // 字符串 - 页面文件路径
      // '/bar/README.md',
    ],
  }),
  themeConfig: {
    sidebar: 'auto',
    editLink: false,
  },
})